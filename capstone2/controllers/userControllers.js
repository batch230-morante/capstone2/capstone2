const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// const user = require("../models/user");
// const product = require("../models/product");

/* module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    console.log(newUser);

    return newUser.save()
    .then(user => {
        console.log(user);
        res.send(true);
    })
    .catch(error => {
        console.log(error);
        res.send(false)
    })

}; */

module.exports.userRegistration = (req, res) =>
{
    let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })

    console.log(newUser);

    return newUser.save()

    .then(user => {
        console.log(user);
        res.send(true);
    })
    .catch(error => {
        console.log(error);
        res.send(false)
    })
}


module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return false
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)};
            }
            else{
                return false;
            }
        }
    })
};


module.exports.getDetails = (reqBody) => {
    return User.findById(reqBody.id).then((result, error) => {
        if(error){
            return false;
        }
        else{
            result.password = "**********"
            return result;
        }
    });
}


module.exports.getProfile = (req, res) => {
		
    const userData = auth.decode(req.headers.authorization);

    console.log(userData);

    return User.findById(userData.id).then(result =>{
        result.password = "***";
        res.send(result);
    })
}


module.exports.getAllUsersController = (request, response) => {
    User.find({})
    .then(result => response.send(result))
    .catch(error => response.send(error));
}


// module.exports.getOrders = () => {
//     return User.orders().then((result, error) => {
//         if(error){
//             return false;
//         }
//         else{
//             result.totalAmount = "**********"
//             return result;
//         }
//     });
// }


module.exports.getOrders = (newData) => {
	if(newData.isAdmin == true){
		return User.orders()
		.then((result, error) => {
			if(error){
				return false;
			}
			else{
				return result;
			}
		})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
	}
}
/*

module.exports.archiveProduct = (_id, newData) => {
    if(newData.isAdmin == true){
    return Product.findByIdAndUpdate(_id,
        {
        isActive: newData.product.isActive,
        }
        ).then((result, error) => {
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}

*/

module.exports.checkEmailExists = (req, res) =>{
	return User.find({email: req.body.email}).then(result =>{

		// The result of the find() method returns an array of objects.
			 // we can use array.length method for checking the current result length
		console.log(result);

		// The user already exists
		if(result.length > 0){
			return res.send(true);
			// return res.send("User already exists!");
		}
		// There are no duplicate found.
		else{
			return	res.send(false);
			// return res.send("No duplicate found!");
		}
	})
	.catch(error => res.send(error));
}