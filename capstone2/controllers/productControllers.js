const mongoose = require("mongoose");
const Product = require("../models/product");
const auth = require("../auth");
const User = require("../models/user");


module.exports.addProduct = (reqBody, newData) => {
    if(newData.isAdmin == true){
        let newProduct = new Product({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price,
            stocks: reqBody.stocks
        })
        return newProduct.save().then((newProduct, error) => {
            if(error){
                return error;
            }
            else{
                return newProduct;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
};

module.exports.getActiveProducts = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    })
};


module.exports.getProduct = (_id) => {
    return Product.findById(_id).then(result => {
        return result;
    })
}

// OG CODE WITHOUT .SAVE BELOW
/*
module.exports.updateProduct = (_id, newData) => {
    if(newData.isAdmin == true){
    return Product.findByIdAndUpdate(_id,
        {
        name: newData.product.name,
        description: newData.product.description,
        price: newData.product.price
        }
        ).then((result, error) => {
            if(error){
                return false;
            }
            else{
                return result;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}
*/
// OG CODE WITHOUT .SAVE ABOVE 

module.exports.updateProduct = (_id, newData) => {
    if (newData.isAdmin == true) {
    return Product.findByIdAndUpdate(_id, {
        name: newData.product.name,
        description: newData.product.description,
        price: newData.product.price,
        stocks: newData.product.stocks
    })
        .then((updatedProduct) => {
        return updatedProduct.save().then(() => {
            return updatedProduct;
        })  ;
        })
        .catch((error) => {
        console.error(error);
        return false;
        });
    } else {
    let message = Promise.resolve(
        "User must be ADMIN to access this functionality"
    );
    return message.then((value) => {
        return value;
    });
    }
};

module.exports.archiveProduct = (_id, newData) => {
    if(newData.isAdmin == true){
    return Product.findByIdAndUpdate(_id,
        {
        isActive: newData.product.isActive,
        }
        ).then((result, error) => {
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}


module.exports.createOrder = async (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	let productName = await Product.findById(request.body._id).then(result => {
        console.log("THIS IS THE RESULT:")
        console.log(result);
        return result});

	let newData = {
		userId: userData.id,
		email: userData.email,
        productId: request.body._id,
        productName: productName,
        quantity: request.body.quantity
	}

	console.log(newData);
    let price = await Product.findById(newData.productId)
    .then((result) => result.price)
    console.log(`This is the ${price}`);

	let isUserUpdated = await User.findById(newData.userId)
	.then(user => {
		user.orders.push({
			productId: newData.productId,
            totalAmount: price * newData.quantity,
            quantity: newData.quantity
		});
        console.log(productName)
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(newData.productId).then(product => {

		product.orders.push({
			email: newData.email,
			quantity: newData.quantity
		})
	
		product.stocks -= newData.quantity;

		return product.save()
		.then(result=>{
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isProductUpdated);

	(isUserUpdated == true &&  isProductUpdated == true)? response.send(true) : response.send(false);
}

// CONTROLLER
module.exports.getAllProducts = (request, response) => {
    return Product.find({})
    .then(result => response.send(result))
    .catch(error => response.send(error));
}


module.exports.unArchiveProduct = (_id, newData) => {
    if(newData.isAdmin == true){
    return Product.findByIdAndUpdate(_id,
        {
        isActive: newData.product.isActive,
        }
        ).then((result, error) => {
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}