const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First Name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is required"]
    },
    email: {
        type: String,
        required: [true, "E-mail address is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
            productId: {
                type: String
            },
            totalAmount: {
                type: Number
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            },
            quantity: {
                type: Number
            }
        }
    ]
});


module.exports = mongoose.model("User", userSchema);