const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    price: {
        type: Number
    },
    stocks: {
        type: Number
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            userEmail: {
                type: String
            },
            quantity: {
                type: Number
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
});


module.exports = mongoose.model("Product", productSchema);