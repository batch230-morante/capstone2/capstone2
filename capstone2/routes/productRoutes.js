const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

router.post("/create", auth.verify, (request, response) => {
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
    productController.addProduct(request.body, newData).then(resultFromController => response.send(resultFromController))
});



router.get("/activeProducts", (request, response) => {
    productController.getActiveProducts().then(resultFromController => response.send(resultFromController))
});



router.get("/:_id", (request, response) => {
    productController.getProduct(request.params._id).then(resultFromController => response.send(resultFromController))
})



router.get("/all", productController.getAllProducts);


router.patch("/:_id/update", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productController.updateProduct(request.params._id, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})



router.patch("/:_id/archive", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productController.archiveProduct(request.params._id, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

router.post("/checkout", auth.verify, productController.createOrder);



router.get("/", productController.getAllProducts);


router.patch("/:_id/unarchive", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productController.unArchiveProduct(request.params._id, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


module.exports = router;