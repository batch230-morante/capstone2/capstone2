const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

/* router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});
 */
router.post("/register", userControllers.userRegistration);

router.post("/login", (request, response) => {
    userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController));
})


router.post("/detailsOld", (request, response) => {
    userControllers.getDetails(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/checkEmail", userControllers.checkEmailExists);

router.get("/allUsers", userControllers.getAllUsersController);


router.get("/allOrders",  userControllers.getAllUsersController);

router.get("/details", auth.verify, userControllers.getProfile);


module.exports = router;
