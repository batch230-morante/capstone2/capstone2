// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// End of dependencies


const userRoutes = require("../capstone2/routes/userRoutes");
const productRoutes = require("../capstone2/routes/productRoutes");

// Create web server application
const app = express();


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
// End of middlewares


mongoose.connect("mongodb+srv://admin:admin@batch230.tkbppxq.mongodb.net/MoranteEcommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to MoranteCapstone-Mongo DB Atlas"));

app.listen(process.env.PORT || 3001, () =>
    {console.log(`API is now online on port ${process.env.PORT || 3001}`)
});